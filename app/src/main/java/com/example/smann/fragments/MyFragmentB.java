package com.example.smann.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by smann on 06/09/2017.
 */

public class MyFragmentB extends Fragment{

    TextView tv = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_fragment_b_ui,
                container, false);

        tv = view.findViewById(R.id.resultText);

        return view;
    }


    public void doCalcDisplay(int a, int b) {

        // perform the addition operation and update the textview in this fragment
        tv.setText("" + (a+b));
    }

}
