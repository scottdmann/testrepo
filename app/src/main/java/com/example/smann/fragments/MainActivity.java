package com.example.smann.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.*;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//
public class MainActivity extends AppCompatActivity {

    Button resButton = null;

    EditText opA = null;
    EditText opB = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       resButton = (Button) findViewById(R.id.button);
        opA = (EditText) findViewById(R.id.firstOperandET);
        opB = (EditText) findViewById(R.id.secondOperandET);

        resButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyFragmentA fA = (MyFragmentA) getFragmentManager().findFragmentById(R.id.fragment);
                MyFragmentB fB = (MyFragmentB) getFragmentManager().findFragmentById(R.id.fragment2);

                int a = Integer.parseInt(opA.getText().toString());
                int b = Integer.parseInt(opB.getText().toString());

                if(a !=0 && b!=0) {
                    fA.doCalcDisplay(a, b);
                    fB.doCalcDisplay(a, b);
                } else {
                    Toast.makeText(getApplicationContext(),"Please enter non-zero integers", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}
